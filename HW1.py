# version1:  integer   np.dot
import numpy as np
import time
from numpy import random
start_time = time.time()
for i in range(100):
    arr1 = random.randint(1,100,size=(600,400))
    arr2 = random.randint(1,100,size=(400,700))
    A = np.dot(arr1,arr2)
end_time = time.time()
print('time is %s' %str(end_time-start_time))
#



# version2:  real number   np.dot
import numpy as np
import time
from numpy import random
start_time = time.time()
for i in range(100):
    arr1 = random.uniform(1,100,size=(600,400))
    arr2 = random.uniform(1,100,size=(400,700))
    A = np.dot(arr1,arr2)
end_time = time.time()
print('time is %s' %str(end_time-start_time))



# version3:  integer    np.multiply
import numpy as np
import time
from numpy import random
start_time = time.time()
for i in range(100):
    arr1 = random.randint(1,100,size=(600,700))
    arr2 = random.randint(1,100,size=(600,700))
    A = np.multiply(arr1,arr2)
end_time = time.time()
print('time is %s' %str(end_time-start_time))


# version4:  real number   np.multiply
import numpy as np
import time
from numpy import random
start_time = time.time()
for i in range(100):
    arr1 = random.uniform(1,100,size=(600,700))
    arr2 = random.uniform(1,100,size=(600,700))
    A = np.multiply(arr1,arr2)
end_time = time.time()
print('time is %s' %str(end_time-start_time))